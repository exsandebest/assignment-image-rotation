#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_FUNCTIONS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_FUNCTIONS_H

#include "image.h"

struct image rotate(struct image const img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_FUNCTIONS_H
