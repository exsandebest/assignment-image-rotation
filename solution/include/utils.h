#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "bmp_header.h"
#include "image.h"

#include <stdint.h>

uint8_t get_padding(uint64_t w);
uint64_t get_pixel_index(uint64_t x, uint64_t y, uint64_t w);
uint64_t get_rotated_pixel_index(uint64_t x, uint64_t y, uint64_t w);
struct bmp_header get_bmp_header(const struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
