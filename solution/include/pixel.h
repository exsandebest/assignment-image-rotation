#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H

#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
#pragma pack(pop)

#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
