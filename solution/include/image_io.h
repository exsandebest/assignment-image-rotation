#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H

#include "image.h"

#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

uint64_t read_bmp_image(const char* file_name, struct image* img);

uint64_t write_bmp_image(const char* file_name, struct image const* img);

enum read_status from_bmp(FILE* file, struct image* img);

enum write_status to_bmp(FILE* file, struct image const* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
