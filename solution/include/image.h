#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include "pixel.h"

#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image();
void destroy_image(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
