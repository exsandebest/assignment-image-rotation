#include "image.h"
#include "image_functions.h"
#include "image_io.h"

#include <stdio.h>

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        return -1;
    }

    struct image img = {0};

    if (read_bmp_image(argv[1], &img) != 0) {
        fprintf(stderr, "Error while reading source file\n");
        return -1;
    }

    struct image img_new = rotate(img);

    if (write_bmp_image(argv[2], &img_new) != 0) {
        fprintf(stderr, "Error while writing target file\n");
        return -1;
    }

    destroy_image(img);
    destroy_image(img_new);

    fprintf(stdout, "Imaged rotated\n");

    return 0;
}
