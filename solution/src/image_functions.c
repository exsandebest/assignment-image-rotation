#include "image_functions.h"
#include "utils.h"

struct image rotate(struct image const img) {
    struct image img_new = create_image(img.height, img.width);

    for (uint64_t i = 0; i < img.height; ++i) {
        for (uint64_t j = 0; j < img.width; ++j) {
            img_new.data[get_rotated_pixel_index(i, j, img_new.width)] = img.data[get_pixel_index(i, j, img.width)];
        }
    }

    return img_new;
}
