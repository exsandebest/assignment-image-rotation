#include "image.h"

#include <stdlib.h>

struct image create_image(uint64_t w, uint64_t h) {
    struct image img = {
            .width = w,
            .height = h,
            .data = malloc((h * w) * sizeof(struct pixel))
    };
    return img;
}

void destroy_image(struct image img) {
    free(img.data);
}
