#include "bmp_header.h"
#include "image.h"
#include "image_io.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

uint64_t read_bmp_image(const char* file_name, struct image* img) {
    FILE* file_in = fopen(file_name, "r");
    if (!file_in) {
        fprintf(stderr, "Error while open source file\n");
        return -1;
    }
    if (from_bmp(file_in, img) != READ_OK) {
        return -1;
    }
    if (fclose(file_in) != 0) {
        fprintf(stderr, "Error while closing source file\n");
        return -1;
    }
    return 0;
}

uint64_t write_bmp_image(const char* file_name, struct image const* img) {
    FILE* file_out = fopen(file_name, "w");
    if (!file_out) {
        fprintf(stderr, "Error while open target file\n");
        return -1;
    }
    if (to_bmp(file_out, img) != WRITE_OK) {
        fprintf(stderr, "Error while writing target file\n");
        return -1;
    }
    if (fclose(file_out) != 0) {
        fprintf(stderr, "Error while closing target file\n");
        return -1;
    }
    return 0;
}

enum read_status from_bmp(FILE* file, struct image* img) {
    struct bmp_header header = {0};
    const size_t read_res = fread(&header, sizeof(struct bmp_header), 1, file);
    if (read_res != 1) {
        fprintf(stderr, "Error while reading header\n");
        return READ_INVALID_HEADER;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc((img->height * img->width) * sizeof(struct pixel));
    const uint8_t padding = get_padding(img->width);

    if (fseek(file, header.bOffBits, SEEK_SET)) {
        fprintf(stderr, "Error while skipping header (reading) - fseek\n");
        return READ_INVALID_BITS;
    }

    for (uint64_t i = 0; i < img->height; ++i) {
        const size_t read_res = fread(&(img->data)[img->width * i], sizeof(struct pixel), img->width, file);
        if (read_res != img->width) {
            fprintf(stderr, "Error while reading pixels (bits)\n");
            return READ_INVALID_BITS;
        }
        if (padding) {
            if (fseek(file, padding, SEEK_CUR)) {
                fprintf(stderr, "Error while reading pixels (bits) - fseek\n");
                return READ_INVALID_BITS;
            }
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* file, struct image const* img) {
    const uint8_t padding = get_padding(img->width);
    const struct bmp_header header = get_bmp_header(img);
    const size_t write_res = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (write_res != 1) {
        fprintf(stderr, "Error while writing header\n");
        return WRITE_ERROR;
    }

    if (fseek(file, header.bOffBits, SEEK_SET)) {
        fprintf(stderr, "Error while skipping header (writing) - fseek\n");
        return WRITE_ERROR;
    }
    const uint32_t zero_byte = 0;

    for (uint64_t i = 0; i < img->height; ++i) {
        const size_t write_res = fwrite(&img->data[img->width * i], sizeof(struct pixel), img->width, file);
        if (write_res != img->width) {
            fprintf(stderr, "Error while writing pixels (bits)\n");
            return WRITE_ERROR;
        }
        if (padding) {
            const size_t write_res = fwrite(&zero_byte, sizeof(uint8_t), padding, file);
            if (write_res != padding) {
                fprintf(stderr, "Error while writing padding (bits)\n");
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
