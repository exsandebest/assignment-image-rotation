#include "bmp_header.h"
#include "image.h"
#include "utils.h"

#define BBC 24
#define BP 1
#define BFT 0x4D42

uint8_t get_padding(uint64_t w) {
    return w % 4;
}

uint64_t get_pixel_index(uint64_t x, uint64_t y, uint64_t w) {
    return w * x + y;
}

uint64_t get_rotated_pixel_index(uint64_t x, uint64_t y, uint64_t w) {
    return w * y + w - x - 1;
}

struct bmp_header get_bmp_header(const struct image* img) {
    const uint8_t padding = get_padding(img->width);
    const struct bmp_header header = {
            .bfType = BFT,
            .bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel) + padding * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 0,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BP,
            .biBitCount = BBC,
            .biCompression = 0,
            .biSizeImage = img->width * img->height * sizeof(struct pixel) + padding * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}
